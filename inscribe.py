#!/usr/bin/env python3
'Draw CV via OSC or via monome crow'
import traceback

try:
    from druid.crow import Crow
except ImportError:
    Crow = None
try:
    from pythonosc import udp_client
except ImportError:
    udp_client = None
import argparse
import signal
import asyncio
from time import perf_counter
import math
from threading import Thread
try:
    from evdev import ecodes, InputDevice, list_devices
except ImportError:
    print('Error accessing evdev. Try installing the python3-evdev package.')
    exit(1)

# Graphics tablet vendors for auto device detect. Tested with Wacom and Huion. Try adding your device?
VENDORS = [
    'wacom',
    'huion',
    'hid 256c'
]

# Set in set_device_ranges
X_MAX = 1
Y_MAX = 1
XY_MAX = 1 # greater of X and Y MAX
PRESSURE_MAX = 1

# the event queue that the event loop writes to and the tx loop reads from
# a dict because the most recent event is what is read
global queue
queue = {}

def parse_args():
    opt = argparse.ArgumentParser(description=__doc__)
    action = opt.add_mutually_exclusive_group(required=True)
    action.add_argument('--crow', action='store_true', help='Send the events to crow.')
    action.add_argument('--osc', action='store_true', help='Send the events to OSC.')
    action.add_argument('--print-caps', action='store_true', help='Pretty-print the capabilities of the selected devices, then exit.')

    opt.add_argument('-i', '--ip', default='127.0.0.1', help='IP address to send OSC to. (default: 127.0.0.1)')
    opt.add_argument('-p', '--port', default=8082, type=int, help='Port to send OSC to. (default: 8082)')
    opt.add_argument('-a', '--address', default='/inscribe', help='OSC address to send to.')

    opt.add_argument('-r', '--rate', default=5, type=int, help='Transmission rate in milliseconds. (default: 5)')

    opt.add_argument('--smooth-velocity', action=argparse.BooleanOptionalAction, default=True,
                     help='Smooth out velocity output at the cost of some latency.')

    opt.add_argument('--auto-devices', action=argparse.BooleanOptionalAction, default=True,
                     help='Automatically find tablet devices.')
    opt.add_argument('-g', '--grab', action=argparse.BooleanOptionalAction, default=True,
                     help='Grab exclusive control over the device(s).')
    opt.add_argument('devices', nargs='*', metavar='DEVICES', help='Kernel addresses for the devices to listen to (eg. /dev/input/event5). Use libinput list-devices to show them. Takes precedence over --auto-devices.')

    args = opt.parse_args()
    if args.ip == 'localhost':
        args.ip = '127.0.0.1'

    return (args, opt)


def has_cap(d, cap, type):
    '''True if a device has a cap and a type.'''
    caps = d.capabilities()
    return type in caps and cap in caps[type]


def get_tablet_devices():
    '''Returns a list of tablet devices (pen & button -- no touch for now).'''

    all_devices = [InputDevice(path) for path in list_devices()]
    tablet_devices = [d for d in all_devices if [True for v in VENDORS if v in d.name.lower()]]

    # only use devices that don't have finger capability
    return list(filter(lambda d: not has_cap(d, ecodes.BTN_TOOL_FINGER, ecodes.EV_KEY), tablet_devices))

    # useful for categorizing devices in the future if necessary
    # pen_dev = [d for d in tablet_devices if has_cap(d, ecodes.BTN_TOOL_PEN, ecodes.EV_KEY)]
    # pad_dev = [d for d in tablet_devices if has_cap(d, ecodes.BTN_FORWARD, ecodes.EV_KEY)]


def pretty_print_capabilities(dev):
    '''Prints the capabilities of the specified device.'''

    caps = dev.capabilities()
    del caps[0] # we don't report SYN events -- they're too much

    # start with the name
    print("\n{} ({})\n====".format(dev.name, dev.path))

    # list all capabilities for each event type
    for type in caps:
        for ev_caps in caps[type]:
            # events are an int if they don't have an associated absinfo,
            # otherwise they're a pair of the int index and the absinfo.
            # we have to duck type them to handle it appropriately.
            if isinstance(ev_caps, int):
                ev_name = ecodes.bytype[type][ev_caps]
                print(ev_name)
            else:
                ev_name = ecodes.bytype[type][ev_caps[0]]
                ev_abs_info = ev_caps[1]
                print("{}: {}".format(ev_name, ev_abs_info))


def set_device_ranges(dev):
    '''Gets & stores the max value of the relevant device inputs.'''

    global X_MAX
    global Y_MAX
    global XY_MAX
    global PRESSURE_MAX

    caps = dev.capabilities()
    for cap in caps[ecodes.EV_ABS]:
        if cap[0] == ecodes.ABS_X:
            X_MAX = cap[1].max
        elif cap[0] == ecodes.ABS_Y:
            Y_MAX = cap[1].max
        elif cap[0] == ecodes.ABS_PRESSURE:
            PRESSURE_MAX = cap[1].max
    XY_MAX = max(X_MAX, Y_MAX)


def linlin(input, in_min, in_max, out_min, out_max):
    '''Linearly scales a value between two ranges.'''

    scale  = (out_max - out_min) / (in_max - in_min)
    offset = out_min - (scale * in_min)
    return (input * scale) + offset


class Parser():
    '''Parses EvDev events and adds them to the global queue.'''
    vel_smoothing = 0.3
    trend_smoothing = 0.1

    def __init__(self):
        self._touching = False
        self._held = {}
        self._x = 0
        self._x_raw = 0
        self._y = 0
        self._y_raw = 0
        self._pressure = 0
        self._queue = {}
        self._dropped = False
        self._prev_pos = { 'x_raw': 0, 'y_raw': 0, 'touching': False }
        self._pos_chg = False
        self._then = 0
        self._vhist = 0
        self._vtrend = 0

    def enqueue(self, key, value):
        '''Enqueues an event to send'''
        self._queue[key] = value

    def flush_queue(self):
        '''Pulishes and then clears the local queue'''
        global queue
        global pos

        touching = self._pressure > 0

        # if touch changes
        if touching != self._touching:
            self.enqueue('touching', 1 if touching else 0)
            self._touching = touching

        if touching:
            self.enqueue('x', self._x)
            self.enqueue('y', self._y)
            self.enqueue('touch', [self._x, self._y, self._pressure])

        if self._pos_chg:
            (vel, angle) = self._get_vector()
            self.enqueue('velocity', vel)
            self.enqueue('angle', angle)
            self._pos_chg = False

        queue = self._queue
        self._queue = {}

    def parse_event(self, event, is_pen):
        '''Sends the data to the consumer using self._send'''

        e_code = event.code
        e_type = event.type
        e_value = event.value
        global X_MAX
        global Y_MAX
        global PRESSURE_MAX
        global pos

        # handle position & pressure events
        if e_type == ecodes.EV_ABS:
            if e_code == ecodes.ABS_X:
                self._x = linlin(e_value, 0, X_MAX, 0, 1)
                self._x_raw = e_value
                self._pos_chg = True
            elif e_code == ecodes.ABS_Y:
                self._y = linlin(e_value, 0, Y_MAX, 0, 1)
                self._y_raw = e_value
                self._pos_chg = True
            elif e_code == ecodes.ABS_PRESSURE:
                self._pressure = linlin(e_value, 0, PRESSURE_MAX, 0, 1)
                self.enqueue('pressure', self._pressure)
        # handle buttons
        elif e_type == ecodes.EV_KEY:
            button_name = ''
            if e_code == ecodes.BTN_RIGHT:
                button_name = 'btn_top_left'
            elif e_code == ecodes.BTN_LEFT:
                button_name = 'btn_bottom_left'
            elif e_code == ecodes.BTN_FORWARD:
                button_name = 'btn_top_right'
            elif e_code == ecodes.BTN_BACK:
                button_name = 'btn_bottom_right'
            elif e_code == ecodes.BTN_STYLUS:
                button_name = 'btn_stylus_lower'
            elif e_code == ecodes.BTN_STYLUS2:
                button_name = 'btn_stylus_upper'
            elif e_code == ecodes.BTN_TOOL_RUBBER:
                button_name = 'btn_eraser'
            else:
                # button_name = ecodes.keys[e_code]
                return # keep the list simple for now
            self.enqueue(button_name, e_value)
        elif e_type == ecodes.EV_MSC and not is_pen:
            if e_code == ecodes.MSC_SCAN:
                self._held[e_value] = 1 - self._held[e_value] if e_value in self._held else 1
                self.enqueue('btn_' + str(e_value - 589825), self._held[e_value])
                pass
            else:
                self.enqueue(ecodes.bytype[e_type][e_code], e_value)
        elif e_type == ecodes.EV_SYN:
            if e_code == ecodes.SYN_REPORT:
                if self._dropped:
                    self._queue = {}
                    self._dropped = False
                else:
                    self.flush_queue()
            elif e_code == ecodes.SYN_DROPPED:
                # ignore events between last SYN_REPORT and the next one
                # https://www.freedesktop.org/software/libevdev/doc/1.1/syn_dropped.html
                self._queue = {}
                self._dropped = True
                print("DROPPED")

    def _get_vector(self):
        '''Calculates and returns (velocity, angle)'''
        vel = 0
        angle = 0
        if self._touching and not self._prev_pos['touching']:
            # when touch starts, velocity is 0, and set time
            self._prev_pos['x_raw'] = self._x_raw
            self._prev_pos['y_raw'] = self._y_raw
            self._prev_pos['touching'] = self._touching
            self._vhist = 0
            self._vtrend = 0
            self._then = perf_counter()
        elif self._touching:
            # calculate velocity if position changes while touching
            now = perf_counter()
            dt = now - self._then
            dx = self._x_raw - self._prev_pos['x_raw']
            dy = self._y_raw - self._prev_pos['y_raw']
            dxy = math.sqrt(pow(dx, 2) + pow(dy, 2))
            vel = dxy / XY_MAX / dt
            angle = math.degrees(math.atan2(dy, dx))
            angle = (angle + 450) % 360 # reorient to degrees clockwise from north

            self._prev_pos['x_raw'] = self._x_raw
            self._prev_pos['y_raw'] = self._y_raw
            self._prev_pos['touching'] = self._touching
            self._then = now
        elif self._prev_pos['touching'] and not self._touching:
            # when touch ends, velocity is 0
            self._prev_pos['touching'] = self._touching

        vel = self._smooth(vel, self.vel_smoothing, self.trend_smoothing)
        return (vel, angle)

    def _smooth(self, val, smoothing, trend_smoothing):
        '''Double exponential smoothing
        https://en.wikipedia.org/wiki/Exponential_smoothing#Double_exponential_smoothing_(Holt_linear)
        '''
        output = val * smoothing + (self._vhist + self._vtrend) * (1 - smoothing)
        trend = (output - self._vhist) * trend_smoothing + self._vtrend * (1 - trend_smoothing)
        self._vhist = output
        self._vtrend = trend
        return output

class Transmitter():
    '''Transmits messages to crow or OSC.

    Transmitter.send() will send events using the stored function self._send,
    which is one of either self._send_crow or self._send_osc, both of which
    have the same signature. Be sure to call set_crow() or set_osc() before send()!
    '''

    def send(self, key, value):
        self._send(key, value)

    def set_crow(self, crow):
        '''Sets the instance to send events to crow. Assumes crow is already connected.'''
        self._crow = crow
        self._send = self._send_crow

    def set_osc(self, osc_client, osc_base_path):
        '''Sets the instance to send events to OSC. Assumes OSC is already connected.'''
        self._osc_client = osc_client
        self._osc_base_path = osc_base_path
        self._send = self._send_osc

    def _send_crow(self, name, value):
        '''The sender for crow.'''
        if isinstance(value, list):
            value = "{" + ",".join([f"{el}" for el in value]) + "}"
        self._crow.writeline(f"public.update('{name}', {value})")

    def _send_osc(self, name, value):
        '''The sender for OSC events.'''
        self._osc_client.send_message("{}/{}".format(self._osc_base_path, name), value)


async def start_device_loop(in_dev, parser, args):
    '''Starts the loop for handling events from an input device.'''
    is_pen = has_cap(in_dev, ecodes.BTN_TOOL_PEN, ecodes.EV_KEY)

    if args.grab:
        with in_dev.grab_context():
            async for event in in_dev.async_read_loop():
                parser.parse_event(event, is_pen)
    else:
        async for event in in_dev.async_read_loop():
            parser.parse_event(event, is_pen)


async def start_tx_loop(tx, ival):
    '''Starts the loop for transmitting events to crow or OSC.'''
    global queue
    while True:
        for name in queue.keys():
            tx.send(name, queue[name])
        queue = {}
        await asyncio.sleep(ival)


async def main():
    (args, arg_parser) = parse_args()

    devices = args.devices

    if not len(devices) and args.auto_devices:
        devices = get_tablet_devices()

    if not len(devices):
        print('No devices selected.')
        arg_parser.print_help()

    try:
        devices = [InputDevice(d) for d in devices]
    except FileNotFoundError as e:
        print('Could not open device. It might be disconnected, in use, or you might not have the correct permissions.')
        print(e)
        exit(1)

    [set_device_ranges(d) for d in devices if has_cap(d, ecodes.BTN_TOOL_PEN, ecodes.EV_KEY)]

    if args.print_caps:
        [pretty_print_capabilities(d) for d in devices]
        return
    else:
        print('Listening for events on these devices:')
        [print(d) for d in devices]


    # Finally start up the async loop to connect device input to crow
    parser = Parser()
    transmitter = Transmitter()
    tasks = []

    if not args.smooth_velocity:
        parser.vel_smoothing = 1
        parser.trend_smoothing = 1

    # need to call this in the with-as block for crow...
    async def await_tasks():
        tasks = [start_device_loop(dev, parser, args) for dev in devices]
        tasks.append(start_tx_loop(transmitter, args.rate / 1000))

        try:
            await asyncio.gather(*tasks)
        except (asyncio.CancelledError, KeyboardInterrupt):
            return
        except Exception as e:
            print(e)
            traceback.print_exc()

    if args.crow:
        if Crow == None:
            print("Could not find druid, please install it: pip3 install monome-druid");
            exit(1)

        with Crow() as crow:
            crow.reconnect()
            transmitter.set_crow(crow)
            await await_tasks()
    elif args.osc:
        if udp_client == None:
            print("Could not find python-osc, please install it: pip3 install python-osc");
            exit(1)

        osc_client = udp_client.SimpleUDPClient(args.ip, args.port)
        transmitter.set_osc(osc_client, args.address)
        await await_tasks()
    else:
        print('No receiver specified, use --crow or --osc.')
        exit(0)


if __name__ == '__main__':
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass # this will apparently be smoother in python 3.11...
    except Exception as e:
        print(e)
        traceback.print_exc()
