# inscribe

## Draw CV via OSC or via monome crow

`inscribe` auto-detects wacom and huion tablets, gathers their events using `evdev`, then sends the relevant control data to either OSC or monome [crow](https://monome.org/docs/crow/) (the latter using the [public API](https://monome.org/docs/crow/reference/#public)).

This project is maintained on a best-effort basis. I encourage you to log bugs if you find them. I can't promise to address them immediately, but I _definitely_ can't address them ever if I don't know about them. `inscribe` reuses a bunch of code & concepts from [draggy](https://gitlab.com/daveriedstra/draggy), check there if you're having trouble or looking for ways to expand it.

## Install

Download the script and run it. You will also need the following packages installed:

- `evdev`: `pip3 install evdev` or `sudo apt install python3-evdev`
- `druid` (if using with monome crow): `pip3 install monome-druid`
    - The script will run without `druid` installed but will not send to crow
- `python-osc` (if using with OSC): `pip3 install python-osc`
    - The script will run without `python-osc` installed but will not send OSC

This script supports Linux and should also work on BSDs that use `evdev`.

## Usage

To send to crow:

```
inscribe.py --crow
```

To send OSC:

```
inscribe.py --osc
```

OSC defaults:

- ip (`-i`): `127.0.0.1` (ie, the local machine)
- port (`-p`): `8082`
- address (`-a`): `/inscribe`

To send OSC somewhere other than the defaults:

```
inscribe.py --osc -i 1.1.1.1 -p 55555
```
### Values

`inscribe` reports the following values:

- `x` and `y` for position coordinates (both scaled 0-1, origin in bottom-left so up is up)
- `pressure` for pen pressure (scaled 0-1)
- `touching` for start and end of pen contact with tablet (1 for touching start, 0 for touching end)
- `touch` as a package of `[x, y, pressure]`
- `velocity` for the speed of the gesture movement (in units / sec, will vary by touchpad model, also see notes below)
- `angle` for the angle of the gesture (in degrees clockwise past noon)
- the following stylus buttons: `btn_stylus_lower`, `btn_stylus_upper`, `btn_eraser`
- the following tablet buttons: `btn_top_left`, `btn_bottom_left`, `btn_top_right`, `btn_bottom_right` (used by Wacom tablets) and `btn_#` (for Huion tablets)

## Notes

- By default, the script will attempt to automatically find a tablet device, but you can optionally provide the device address to the script (something like `/dev/input/event23`). You can find the right one by scanning the output of `libinput list-devices`) ([fzf-input](https://gitlab.com/daveriedstra/fzf-input/) might be of interest).
- The script sends events at a regular rate controlled by the `-r / --rate` argument. If you're getting `event queue full!` from druid, try increasing this value. If you want a snappier feel and your consuming script can handle more events, decrease this number.
- The script cannot send to crow and OSC simultaneously
- There is a known bug where exiting requires two SIGINTs (you'll have to press `ctrl+c` twice).

### Velocity

The velocity calculations are smoothed out internally in order to prevent jumps in the values. This comes at the cost of a small amount of latency. Smoothing can be disabled with the `--no-smooth-velocity` argument.

In some cases, certain crow scripts can interact weirdly with `inscribe`'s velocity calculations, manifesting as strangely high values (along with poor performance on crow). To avoid this, try not to do too much work in the `action` callbacks corresponding to `public` values being updated by inscribe, especially `public{velocity}`.

## Help text

```
usage: inscribe.py [-h] (--crow | --osc | --print-caps) [-i IP] [-p PORT]
                   [-a ADDRESS] [-r RATE]
                   [--smooth-velocity | --no-smooth-velocity]
                   [--auto-devices | --no-auto-devices]
                   [-g | --grab | --no-grab]
                   [DEVICES ...]

Draw CV via OSC or via monome crow

positional arguments:
  DEVICES               Kernel addresses for the devices to listen to (eg.
                        /dev/input/event5). Use libinput list-devices to show
                        them. Takes precedence over --auto-devices.

options:
  -h, --help            show this help message and exit
  --crow                Send the events to crow.
  --osc                 Send the events to OSC.
  --print-caps          Pretty-print the capabilities of the selected devices,
                        then exit.
  -i IP, --ip IP        IP address to send OSC to. (default: 127.0.0.1)
  -p PORT, --port PORT  Port to send OSC to. (default: 8082)
  -a ADDRESS, --address ADDRESS
                        OSC address to send to.
  -r RATE, --rate RATE  Transmission rate in milliseconds. (default: 5)
  --smooth-velocity, --no-smooth-velocity
                        Smooth out velocity output at the cost of some
                        latency. (default: True)
  --auto-devices, --no-auto-devices
                        Automatically find tablet devices. (default: True)
  -g, --grab, --no-grab
                        Grab exclusive control over the device(s). (default:
                        True)
```
